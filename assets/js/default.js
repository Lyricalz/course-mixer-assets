$(document).ready(function() {

    var $this;
    var data;

    $('.selectBox').selectBox();
    $('.load-more a').on('click', (function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.addClass('spin-infinite');
        data = {
            'data-offset': $('.course-items').attr('data-offset')
        };
        $.ajax({
            url: base_url + 'load_more',
            type: 'POST',
            data: data,
            success: function(html) {
                $this.removeClass('spin-infinite');
                $('.load-more').before(html);
                $('.course-items').attr('data-offset', parseInt(data['data-offset']) + 10);
            }
        });
    }));
    function dataToggle() {
        $('[data-toggle]').on('click', (function(e) {
            e.preventDefault();
            $this = $(this);
            var toggleVal = $this.attr('data-toggle');
            $this.parents(toggleVal).toggleClass('active');
            $this.parents(toggleVal).find('[data-toggle-target]').toggleClass('hide');
        }));
    }
    dataToggle();
    /*
     * $('.enquire').submit(function(e) 
     e.preventDefault();
     data = $(this).serializeArray();
     $.post(base_url + 'enquiry/add', data, function(json) {
     if(json == 'added') {
     $('.enquiry-button a').addClass('glow');
     setTimeout(function() {
     $('.enquiry-button a').removeClass('glow');
     }, 500);
     }
     }, 'json');
     });
     */

    $('#checkAll').change(function(e) {
        if ($(this).is(':checked')) {
            $('.checkAllParent').find('input[type=checkbox]').attr('checked', 'checked');
        }
        else {
            $('.checkAllParent').find('input[type=checkbox]').removeAttr('checked', '');
        }
    });

    $('.course-classes > a').click(function(e) {
        $this = $(this);
        $.ajax({
            url: base_url + 'return-view/single-class',
            type: 'post',
            success: function(html) {
                if ($this.hasClass('next-classes')) {
                    $('.course .classes-wrap').prepend(html);
                }
                else {
                    $('.course .classes-wrap').append(html);
                }
                dataToggle();
            }
        });
        e.preventDefault();
    });

    $('.course .prev-classes').click(function(e) {
        $.ajax({
            url: base_url + 'return-view/single-class',
            type: 'post',
            success: function(html) {
                //$('.course .classes-wrap').append(html);
                dataToggle();
            }
        });
        e.preventDefault();
    });

    $('.course-typeahead').typeahead({
        source: function(query, process) {
            return $.getJSON(base_url + 'search.php',
                    {query: query},
            function(data) {
                return process(data);
            });
        }
    });

    function getView(page) {
        $.ajax({
            url: base_url + 'return-view/' + page,
            type: 'post',
            success: function(html) {
                return html;
            }
        });
    }

    $('.register #photo').change(function() {
        data = {
            file: $(this).val(),
            page: content
        };

        $.ajax({
            url: base_url + 'processFileUpload/' + content,
            type: 'post',
            success: function(data) {

            }
        });

    });

    $('.searchText').change(function() {
        $this = $(this);
        $this.parents('.toggle-parent').find('.search-val').text($this.val());
    });

    $('a#courseLocationButton').click(function(e) {
        e.preventDefault();
        if (navigator.geolocation) {
            var timeoutVal = 10 * 1000 * 1000;
            navigator.geolocation.getCurrentPosition(
                    displayPosition,
                    displayError,
                    {enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0}
            );
        }
        else {
            alert("Geolocation is not supported by this browser");
        }
    });

    function displayPosition(position) {
        var lat = position.coords.latitude;
        var long = position.coords.longitude;
        $.post(base_url + 'get_location', {lat: lat, long: long},
        function(data) {
            console.log(lat);
            console.log(long);
            console.log(data);
            $('input#search-location').val(data.area);
            $('.search-menu .search-location .search-val').text(data.area);
            $('input#location-hidden').val(data.area2);

        },
                'json'
                );
    }
    function displayError(error) {
        var errors = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout'
        };
        alert("Error: " + errors[error.code]);
    }

});